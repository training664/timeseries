package com.zoho.crm.TestAPI.Controller

import com.zoho.crm.TestAPI.Repository.DBRepo
import org.springframework.web.bind.annotation._
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.http.ResponseEntity
import org.springframework.http.HttpStatus
import com.zoho.crm.core.util.ZosActions
import com.zoho.crm.TestAPI.Utils.TestRequest
import play.api.libs.json._

import java.util.logging.Level
import java.util.logging.Logger
import com.adventnet.crm.api.util.APIError
import com.zoho.crm.TestAPI.Utils._
import com.zoho.crm.common.utils.CommonUtils
import com.zoho.crm.TestAPI.Repository._
import java.util.List

import com.zoho.crm.TestAPI.Utils.TimeSeries
import com.zoho.crm.TestAPI.Utils.TestRequest

import play.libs.Json._

import com.adventnet.persistence.WritableDataObject
import com.zoho.bean.ScopedAccess
import com.zoho.bean.TransactionType

@RestController
@RequestMapping(path = Array("/crm/zia/testapi"))
class TestController {

  private val TestAPILogger: Logger = Logger.getLogger(this.getClass.getName)

  val testResponseTemplate = new TestResponseTemplate
  val jsonConvertor = new JsonConversionUtil

  @PostMapping(value = Array("/putStats"))
  def putUserActivityDatainZos(@RequestBody requestString: String): ResponseEntity[String] = {
    
    TestAPILogger.log(Level.INFO, "ENTERED putUserActivityDatainZos")
    
    try {
      // Converting Request String to scala object 
      val requestObject = jsonConvertor.getRequestObject(requestString)
      TestAPILogger.info("RequestObject obtained: " + requestObject + "-----")
      val zgid = "15942818"
      val ZosActionResult = execute(requestObject)
//      if (ZosActionResult != null) {
//        TestAPILogger.info("ZOS ACTION RESULT: " + ZosActionResult)
//        val responseObject = jsonConvertor.getResponseObject(ZosActionResult)
//        new ResponseEntity[String](responseObject, HttpStatus.OK)
//      } else {
//        new ResponseEntity[String](testResponseTemplate.TestErrorResponse("No Content"), HttpStatus.NO_CONTENT)
//      }
      
      val responseObject = "Data stored in zos "
      new ResponseEntity[String](responseObject, HttpStatus.OK)
    } catch {
      case th: Throwable => {
        TestAPILogger.severe("Exception_" + th.getStackTraceString)
        val throwable = CommonUtils.getStackTraceString(th)
        new ResponseEntity[String](testResponseTemplate.TestErrorResponse(throwable), HttpStatus.INTERNAL_SERVER_ERROR)
      }

    }
  }

  def execute(requestObj: TestRequest): String = {
    try {
      ScopedAccess.getInstance[DBRepo](classOf[DBRepo], "15942818", TransactionType.SUPPORT)
        .asInstanceOf[DBRepo].process()
    } catch {
      case e: Exception => TestAPILogger.info(s"\n\n exception $e \n\n")
    }
    "SUCCESS"
  }

  @GetMapping(value = Array("/getStats"))
  def getUserActivityDatafromZos(@RequestParam("configId") configId: String): ResponseEntity[String] = {
    
    TestAPILogger.log(Level.INFO, "ENTERED gettUserActivityDatainZos")
    
    try {
      TestAPILogger.info("String input config Id recieved:  " + configId)
      val newconfigId = configId.replaceAll("\"", "")
      TestAPILogger.info(" NEW config Id:  " + newconfigId)
      var Zosdata1 = ZosActions.getDataObject[WritableDataObject]("common-bucket", newconfigId, null, null)
      var Zosdata = Zosdata1.toString()
      if (Zosdata != null) {
        TestAPILogger.info("Test API response obj: ") // + Zosdata + "----")
        val jsonResponse: JsValue = Json.parse(Zosdata)
        TestAPILogger.info("Test API response json :\n" + jsonResponse)
        val strJsonResponse = Json.stringify(jsonResponse)

        TestAPILogger.info("Time Series response json to String :\n" + strJsonResponse)
        new ResponseEntity[String](strJsonResponse, HttpStatus.OK)
      } else {
        new ResponseEntity(" GET DATA FROM ZOS UNSUCCESSFULL", HttpStatus.NO_CONTENT)
      }
    } catch {
      case th: Throwable => {
        TestAPILogger.severe("Exception_" + th.getStackTraceString)
        val errorMessage = "Internal Server Error"
        new ResponseEntity(th.getMessage, HttpStatus.INTERNAL_SERVER_ERROR)
      }
    }
  }

  @DeleteMapping(value = Array("/deleteStats"))
  def deleteUserActivityDatafromZos(@RequestBody requestString: String): ResponseEntity[String] = {
    
    TestAPILogger.log(Level.INFO, "ENTERED deleteUserActivityDatainZos")
    
    try {
      val json = Json.parse(requestString)
      val configId = (json \ "configId").as[String]
      TestAPILogger.info("String extracted from json Input:  " + configId)
      val status = ZosActions.deleteObject("common-bucket", configId, null)
      if (status != null) {
        TestAPILogger.info("Test API response obj: " + status)
        val jsonResponse: JsValue = Json.parse(status.toString)
        TestAPILogger.info("Test API response json :\n" + jsonResponse)
        val strJsonResponse = Json.stringify(jsonResponse)
        TestAPILogger.info("Time Series response json to String :\n" + strJsonResponse)
        new ResponseEntity[String](strJsonResponse, HttpStatus.OK)
      } else {
        new ResponseEntity("{\"status\" : \"Response object is empty\"}", HttpStatus.NO_CONTENT)
      }
    } catch {
      case th: Throwable => {
        TestAPILogger.severe("Exception_" + th.getStackTraceString)
        val errorMessage = "Internal Server Error"
        new ResponseEntity(th.getMessage, HttpStatus.INTERNAL_SERVER_ERROR)
      }
    }
  }
}




/*
 * 
 PUT:		http://localhost.csez.zohocorpin.com:8080/crm/zia/testapi/putStats
 {
    "configId": "123456",
    "period": "day",
    "period_id": 5,
    "do_aggregate": true,
    "use_db_data": false,
    "timeSeries": {
        "time": [1,2
        ],
        "target": [3,4
        ]
    }
	}
 * 
 * 
 */

