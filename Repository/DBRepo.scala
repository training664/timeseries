package com.zoho.crm.TestAPI.Repository

import com.adventnet.adventnetcrmsfa.CRMCALL
import com.adventnet.db.api.RelationalAPI
import com.adventnet.ds.query.{Column, SelectQueryImpl, Table}
import com.adventnet.adventnetcrmsfa.CRMZIAMODELMETA
import com.adventnet.persistence.{DataObject, Row, DataAccess}
import com.zoho.crm.TestAPI.Utils.{TestRequest, TimeSeries}
import com.zoho.crm.core.components.PersistenceCacheUtil
import com.zoho.crm.core.util.ZosActions
import com.zoho.crm.data.{DataSetProvider, PrestoClientObject, QueryType}
import com.zoho.crm.rabbitmq.wrapper.PublisherWrapper
import com.zoho.crm.data.TempDSInfo
import com.zoho.crm.feature.prediction.v3.PredictionUtils
import com.zoho.crm.feature.prediction.v3.meta.{MetaInfoV3, RealTimeData, ModelStatusInfo}
import java.util.logging.{Level, Logger}
import scala.collection.JavaConverters.asScalaIteratorConverter
import scala.collection.mutable.HashMap

class DBRepo {
  
  private val DBRepoLogger: Logger = Logger.getLogger(this.getClass.getName)
  
  
  def getData() : DataObject = {
    
      DBRepoLogger.info("Reached getData")
      
      val persistence = PersistenceCacheUtil.getNonCachePersistenceHandle
      val selectQuery = new SelectQueryImpl(Table.getTable(CRMCALL.TABLE))
      selectQuery.addSelectColumn(Column.getColumn(null, "*"))
  	  val selectQueryResult = RelationalAPI.getInstance().getSelectSQL(selectQuery)
  	  DBRepoLogger.info("Count is:" + selectQueryResult)
      var dataObj = persistence.get(selectQuery)
      DBRepoLogger.info(s"\n\n DBObject size - ${dataObj.size(CRMCALL.TABLE)} \n\n")
      val rowItr = dataObj.getRows(CRMCALL.TABLE).asScala
      DBRepoLogger.info(s"\n\n rowItr size - ${rowItr.size} \n\n")
      dataObj
   }
   
  
   def putData(configId : Long) {
     
      DBRepoLogger.info("Reached putData")
      
      var metaInfo = "{'success': True, "
      metaInfo += "'anomaly_scores': array([8.22734786e-02, 1.00000000e+00, 5.31792709e-02, 3.64001601e-01, "
      metaInfo += "4.46456347e-01, 3.59115718e-04, 1.32362335e-01, 8.27234489e-03,"
      metaInfo += "4.42832677e-02, 3.60352996e-01, 6.54219632e-02, 4.37352067e-02,"
      metaInfo += "1.64300803e-01, 0.00000000e+00, 3.23513130e-03, 8.96809391e-05]), "
      metaInfo += "'threshold': array([0.4285886]), "
      metaInfo += "'model': <src.time_series_anomaly_detection.anomaly_detectors.auto_regressor.AutoRegressor object at 0x1719272b0>, "
      metaInfo += "'min_max_scaler': MinMaxScaler(), "
      metaInfo += "'lags_used': 7,"
      metaInfo += "'next_run_time': Timestamp('2022-12-21 00:00:00', freq='D'), "
      metaInfo += "'next_run_time_in_millis': 1671580800000"
      val persistence = PersistenceCacheUtil.getPersistenceLiteHandle
      val row = new Row(CRMZIAMODELMETA.TABLE)
      row.set(CRMZIAMODELMETA.CONFIGID, configId)
      row.set(CRMZIAMODELMETA.CONFIGTYPE, 1)
      row.set(CRMZIAMODELMETA.CONFIGRELATEDINFO, metaInfo)
      row.set(CRMZIAMODELMETA.MODELSTATUS, 1)
      row.set(CRMZIAMODELMETA.TOTALINFERENCE, 1)
      row.set(CRMZIAMODELMETA.SUCCESSFULINFERENCE, 1)
      row.set(CRMZIAMODELMETA.MODELACCURACY, 90.09)
      DBRepoLogger.info("\n\n ROWS ASSIGNED \n\n")
      val dataObj = persistence.constructDataObject()
      dataObj.addRow(row)
      persistence.add(dataObj)
   }
  
   def convert(dataObject : DataObject): TimeSeries = {
     
      DBRepoLogger.info("REACHED CONVERT")
      
  		var timeArray = new Array[BigDecimal](50)
      var targetArray = new Array[BigDecimal](50)
  		var rowIt = dataObject.getRows(CRMCALL.TABLE).asScala
  		var count = 0
      while(rowIt.hasNext)
    	{ 
          DBRepoLogger.info("REACHED CONVERT")
    			var row = rowIt.next.asInstanceOf[Row]
          DBRepoLogger.info("ROW CONVERTED")
    			var callId = row.get(CRMCALL.CALLID).asInstanceOf[Long]
          DBRepoLogger.info("CALL ID IS " + callId)
    			timeArray(count) = callId
          targetArray(count) = callId
          count+=1
    	}
      val data = TimeSeries(timeArray.toList , targetArray.toList)
      data
   }
   
   def store(data : TimeSeries): String = {
      
      try{
         DBRepoLogger.info("STORE REACHED")
         //ZosActions.putObject("common-bucket", "15942818", data) 
         DBRepoLogger.info("STORE SUCCESS")
      }
      catch{
          case e: Exception => DBRepoLogger.info(s"\n\n exception $e \n\n")
      }
      "SUCCESS"
   }
   
   def writeinHDFS(requestObject : TestRequest, zgId : String) : TempDSInfo = {
      
      val dspObj = new DataSetProvider
      val hdfsPath = "TimeSeriesAnalysis/" + zgId + "/" + requestObject.configId + "_pbdata.parquet"
      DBRepoLogger.info("In writeinHDFS")
      val selectQuery = "Select CALLSTARTDATETIME as Date,Count(*) as CallCount from CrmCall WHERE (((CrmCall.CALLID >= 111112000000000000) AND (CrmCall.CALLID <= 111112999999999999)) OR ((CrmCall.CALLID >= 0) AND (CrmCall.CALLID <= 999999999999))) GROUP BY CALLSTARTDATETIME"
      val dSInfo = dspObj.writeDataSetToTempHDFS( zgId, PrestoClientObject(query = selectQuery, writePath =hdfsPath, queryType =QueryType.PB_MODEL_TRAINING_QUERY))
      DBRepoLogger.info("DSINFO::" + dSInfo.path + "::")
      dSInfo
   }
   
   def publishinRMQ(dSInfo : TempDSInfo){
      val status=PublisherWrapper.publish("time_series_queue",dSInfo.path)
      DBRepoLogger.info("PUBLISHEDINRMQ::" + status)
      status
   }
   
   def process() : String =  {
      
      var time : List[BigDecimal] = List(1,2,3,4,5,6,7,8)
      var target : List[BigDecimal] = List(3,4,12,34,56,74,6,43)
      var timeSeries = new TimeSeries(time, target)
      var requestObj = new TestRequest("1236", "day", 5, false, true, timeSeries)
      DBRepoLogger.log(Level.INFO, "REACHED PROCESS")
      val dataObject = getData()
      DBRepoLogger.log(Level.INFO, "RETURNED FROM GET DATA :: DATA SIZE :: " + dataObject.size("CrmPotential")+ "<--")
      DBRepoLogger.info("Data Fetch Successful ::" + dataObject.toString() + ":: ")
      var data = convert(dataObject)
      var timeArray = new Array[BigDecimal](50)
      var targetArray = new Array[BigDecimal](50)
      if(requestObj.time_series.getTime().length >= 10){
                var time = requestObj.time_series.getTime()
                var target = requestObj.time_series.getTarget()
                var iter = 0
                DBRepoLogger.info("TIME  1::: ")
                while(iter<10){
                  timeArray(iter) = time(iter)
                  targetArray(iter) = target(iter)
                  iter += 1
                }
      }
      else if(requestObj.do_aggregate == true){
          var time = requestObj.time_series.getTime()
          var target = requestObj.time_series.getTarget()
          var iter = 0
          DBRepoLogger.info("TIME  2::: ")
          while(iter<10){
            timeArray(iter) = time(0)
            targetArray(iter) = target(0)
          }
      }
      else if(requestObj.use_db_data == true){
          var rowIt = dataObject.getRows(CRMCALL.TABLE).asScala
          var DBtime = data.getTime()
          var DBtarget = data.getTarget()
          var iter = 0
          var count = 0
          while(iter<10){
            timeArray(iter) = DBtime(count)
            targetArray(iter) = DBtarget(count)
            count += 1
            iter += 1
            if(count>=rowIt.size) count = 0
          }
      }
      else{
        DBRepoLogger.info("ERRORRR :: ")
        // EXception
      }
      data = TimeSeries(timeArray.toList.slice(0, 10),targetArray.toList.slice(0, 10))
      DBRepoLogger.info("CONVERTED DATA::: " + data + ":: ")
      val storageResult = store(data)
      val zgId = "15942818"
      val dSInfo = writeinHDFS(requestObj, zgId)
      val status = publishinRMQ(dSInfo)
      DBRepoLogger.info("STORAGE RESULT:: " + storageResult + ":: " + status + "::")
      // write in RMQ
      return storageResult
   }

}