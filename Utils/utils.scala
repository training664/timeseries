package com.zoho.crm.TestAPI.Utils

import com.fasterxml.jackson.annotation.JsonProperty


package object utils {
  case class SuccessResponse(
    @JsonProperty status: String,
    @JsonProperty code: String,
    @JsonProperty response: Any)

  case class ErrorResponse(
    @JsonProperty status: String,
    @JsonProperty code: String,
    @JsonProperty message: Any)

  case class FailureResponse(
    @JsonProperty status: String,
    @JsonProperty code: String,
    @JsonProperty message: Any) extends Response
    
  trait Response
}