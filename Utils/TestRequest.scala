package com.zoho.crm.TestAPI.Utils

import scala.collection.mutable.HashMap

case class TimeSeries(time : List[BigDecimal], target : List[BigDecimal]){
    def getTime(): List[BigDecimal]={
      return time
    }
    def getTarget(): List[BigDecimal]={
      return target
    }
}

case class TestRequest(configId : String, 
    period: String, 
    period_id : Int, 
    do_aggregate : Boolean, 
    use_db_data : Boolean,
    time_series :  TimeSeries)
{
  
  def getconfigId() : String = {
    return this.configId
  }
  
  def getperiod() : String = {
    return this.period
  }
    
  def getperiod_id() : Int = {
    return this.period_id
  }
    
  def getdo_aggregate() : Boolean = {
    return this.do_aggregate
  }
  
  def getuse_db_data() : Boolean = {
    return this.use_db_data
  }
    
  def gettime_series() : TimeSeries = {
    return this.time_series
  }

  
}