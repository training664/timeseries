package com.zoho.crm.TestAPI.Utils

import com.zoho.crm.TestAPI.Repository.DBRepo
import com.zoho.crm.core.util.ZosActions
import java.util.logging.Level
import java.util.logging.Logger
import com.zoho.bean.ScopedAccess
import com.zoho.bean.TransactionType


object ZosAction {
  
  
    val dbrepo = new com.zoho.crm.TestAPI.Repository.DBRepo
    private val ZosActionLogger: Logger = Logger.getLogger(this.getClass.getName)
  

  
    def execute(requestObj: TestRequest): String = {
      try {
        ScopedAccess.getInstance[DBRepo](classOf[DBRepo], "15942818", TransactionType.SUPPORT)
          .asInstanceOf[DBRepo].process()
      }
      catch {
        case e: Exception => ZosActionLogger.info(s"\n\n exception $e \n\n")
      }
      "SUCCESS"
    }
  
}