package com.zoho.crm.TestAPI.Utils

import java.util.logging.Logger

import org.json.JSONObject
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.JsError
import play.api.libs.json.JsPath
import play.api.libs.json.JsSuccess
import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.libs.json.Reads
import play.api.libs.json._
import play.api.libs.functional.syntax._
import com.google.gson.Gson


class JsonConversionUtil {

  val testResponseTemplate = new TestResponseTemplate
  private val TestAPILogger: Logger = Logger.getLogger(this.getClass.getName)

  def getRequestObject(requestString: String): TestRequest = {
    TestAPILogger.info("Request String: " + requestString + "------------------")
//    
//        implicit val timeSeries: Reads[TimeSeries] = (
//        (JsPath \ "time" ).read[List[Int]] and 
//        (JsPath \ "target").read[List[Int]]
//        )(TimeSeries.apply _)
//        
//        TestAPILogger.info("Request String 01: " + timeSeries + "------------------")
//        
//        implicit val requestObject: Reads[TestRequest] = (
//          (JsPath \ "configId").read[String] and  
//          (JsPath \ "period").read[String] and
//          (JsPath \ "period_id").read[Int] and
//          (JsPath \ "do_aggregate").read[Boolean] and
//          (JsPath \ "use_db_data").read[Boolean] and
//          (JsPath \ "time_series" ).read[TimeSeries])(TestRequest.apply _)
//          
//        val json = Json.parse(requestString)
//        json.validate[TestRequest] match {
//          case JsSuccess(returnObject, _) => {
//            returnObject
//          }
//          case e: JsError => {
//            return null
//          }
//        }
    

    
      val jsonObject = new JSONObject(requestString)
      val configId = jsonObject.getString("configId")
      TestAPILogger.info("Got configId " + configId + "------------------")
      val period = jsonObject.getString("period")
      TestAPILogger.info("period:: " + period + "------------------")
      val period_id = jsonObject.getInt("period_id") 
      TestAPILogger.info("period id:: " + period_id + "------------------")
      val do_aggregate = jsonObject.getBoolean("do_aggregate")  
      TestAPILogger.info("do_aggregate:: " + do_aggregate + "------------------")
      val use_db_data = jsonObject.getBoolean("use_db_data")
      TestAPILogger.info("Got use_db_data " + use_db_data + "------------------")
//      val timeSeriesString = jsonObject.getString("timeSeries")
//      val timeSeriesjsonObject = new JSONObject(timeSeriesString)
//      TestAPILogger.info("timeSeriesString " + timeSeriesString + "------------------")
//      val gson = new Gson
//      val timeSeriesObject = gson.fromJson(timeSeriesString, classOf[TimeSeries])
      val bestTimeRequestobj = new TestRequest(configId, period, period_id, do_aggregate, use_db_data, null)
      bestTimeRequestobj
    
  }

  def getResponseObject(ZosActionResult: String): String = {
    val jsonResponse: JsValue = Json.parse(ZosActionResult)
    //        TestAPILogger.info("Time Series response json :\n" + jsonResponse)
    val strJsonResponse = Json.stringify(jsonResponse)
    //        TestAPILogger.info("Time Series response json to String :\n" + strJsonResponse)
    val responseObject = testResponseTemplate.TestSuccessResponse(strJsonResponse)
    return responseObject
  }

}