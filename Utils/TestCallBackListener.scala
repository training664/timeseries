package com.zoho.crm.TestAPI.Utils

import com.zoho.crm.intelligence.common.util.PythonNotificationListener
import java.util.logging.Logger
import com.google.gson.Gson
import play.api.libs.json.Json
import play.api.libs.json.JsValue
import com.zoho.crm.TestAPI.Repository.DBRepo



class TestCallBackListener extends PythonNotificationListener {
  
 private val LOG = Logger.getLogger(this.getClass.getName)

 override def execute(zgId : String, zosPath : String) : String = {

   LOG.info("\n\n\n\nTestListener :: notification recvd :: \n\n")
   LOG.info("ZGID is :: " + zgId + "ZOSPATH is :: " + zosPath + "\n\n\n")
   val dBRepo = new DBRepo
   dBRepo.putData(15942818)
   LOG.info("\n\nCOMPLETED PUTDATA\n\n")
   var respJson = Json.obj()
   respJson.toString()
 }
  
}